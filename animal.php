<?php
class Animal
{
    public $name = "nama";
    protected $legs = 4;
    public $cold_blooded = "no";
    public function __construct($name)
    {
        return $this->name = $name;
    }
    public function getLegs()
    {
        return $this->legs;
    }
}
