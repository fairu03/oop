<?php
//equire('animal.php');
class Frog extends Animal
{
    public $name = "nama";
    protected $legs = 4;
    public $cold_blooded = "no";
    protected $jump = "hop-hop";
    public function __construct($name)
    {
        $this->name = $name;
    }
    public function jump()
    {
        return $this->jump;
    }
    public function getLegs()
    {
        return $this->legs;
    }
}
