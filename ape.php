<?php
//equire('animal.php');
class Ape extends Animal
{
    public $name = "nama";
    protected $legs = 2;
    public $cold_blooded = "no";
    protected $yell = "Auooo";
    public function __construct($name)
    {
        $this->name = $name;
    }
    public function yell()
    {
        return $this->yell;
    }
    public function getLegs()
    {
        return $this->legs = 2;
    }
}
